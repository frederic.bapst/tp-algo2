package s14;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//======================================================================
public class LempelZiv {
  //======================================================================
  static class LzvCodeTable {
    // Here we store the data two different ways, so that we can efficiently
    // lookup using either the entryNumber, or the contents (byte array)
    private final Map<ArrayList<Byte>, Integer> map;
    private final ArrayList<byte[]> table;
    
    public LzvCodeTable() {
      table = new ArrayList<>();
      table.add(new byte[0]);              // dummy "empty string" entry
      map = new HashMap<>();
      map.put(new ArrayList<Byte>(), 0);   // dummy "empty string" entry
    }

    private int lookup(byte[] s) {
      return map.getOrDefault(asArrayList(s), -1);
    }

    private static ArrayList<Byte> asArrayList(byte[] s) {
      ArrayList<Byte> as = new ArrayList<>();
      for(byte b:s) as.add(b);
      return as;
    }

    public int size() { return map.size(); }

    public boolean contains(byte[] s) {
      return map.containsKey(asArrayList(s));
    }

    public int putAndCode(byte[] s, byte c) {
      int i = lookup(s);
      byte[] z=addToByteArray(s, c);
      table.add(z);
      map.put(asArrayList(z), map.size());
      return i;  
    }

    public byte[] putAndDecode(int index, byte c) {      
      byte[] s = table.get(index);
      byte[] z=addToByteArray(s, c);
      table.add(z);
      map.put(asArrayList(z), map.size());
      return z;
    }

    public String toString() {
      String s = "";
      for (int i=0; i<table.size(); i++) {
        byte[] b = table.get(i);
        String w = "";
        for(byte c:b) w+=Character.isLetterOrDigit((char)c) ? (char)c : "-";
        s += "" + i + ": " + Arrays.toString(b) + " \"" + w + "\"\n";
      }
      return s;
    }
  }
  //======================================================================
  static class LzvItem {
    final int  entryNb;
    final byte appendedChar;
    
    public LzvItem(int entryNb, byte appendedChar) {
      this.entryNb = entryNb;
      this.appendedChar = appendedChar;
    }
    @Override public boolean equals(Object o) {
      if (o == null || this.getClass() != o.getClass()) return false;
      LzvItem oo = (LzvItem)o;
      return oo.entryNb==entryNb && oo.appendedChar==appendedChar;
    }
  }
  //======================================================================

  public static void code(String infile, String outfile) throws IOException  {
    FileInputStream  bis  = new FileInputStream(infile);
    FileOutputStream bos = new FileOutputStream(outfile);
    PrintWriter      tos = new PrintWriter(new FileWriter(outfile+".txt"));
    LzvCodeTable t = new LzvCodeTable();
    int crt = bis.read(); 
    byte [] prefix=new byte[0];
    while(crt != -1) {
      byte crtByte = (byte)crt;
      // TODO - A COMPLETER
      // ...
      // ...  LzvItem item = new LzvItem(...
      // ...  printItem(bos, tos, item);
      // ...
      // Remember there are two cases where EOF can happen:
      // - just after a fresh item;
      // - with a current word that forms a known prefix; in this case
      //   we will add again this word in the code table.
    }
    bis.close();
    bos.close();
    tos.close();
    System.out.println("Code table: size = "+t.size());
    if (t.size() < 10) {
      System.out.println("Table Content:");
      System.out.println(t);
    }
  }

  public static void decode(String infile, String outfile) throws IOException {
    FileInputStream  bis = new FileInputStream(infile);
    Scanner          tis = new Scanner(new FileReader(infile+".txt"));
    FileOutputStream bos = new FileOutputStream(outfile);
    LzvCodeTable t = new LzvCodeTable();
    LzvItem item = readItem(bis, tis);
    while(item != null) {    
      byte[] s = t.putAndDecode(item.entryNb, item.appendedChar);
      bos.write(s); 
      item=readItem(bis, tis);
    }
    bis.close();
    tis.close();
    bos.close();
    System.out.println("Code table: size = "+t.size());
    if (t.size() < 10) {
      System.out.println("Table Content:");
      System.out.println(t);
    }
  }

  private static byte[] addToByteArray(byte [] t, byte b) {
    byte [] res=new byte[t.length+1];
    System.arraycopy(t, 0, res, 0, t.length);
    res[res.length-1] = b;
    return res;
  }

  private static void printItem(FileOutputStream bos, 
                                PrintWriter      tos,
                                LzvItem          item) throws IOException {
    printItemAsBinary(bos, item);  // really compressed!
    printItemAsText  (tos, item);  // useful for debugging
  }

  private static void printItemAsBinary(FileOutputStream bos, LzvItem item) throws IOException {
    // TODO - A COMPLETER
  }

  private static void printItemAsText( PrintWriter tos,  LzvItem item) throws IOException {
    tos.print(" "+item.entryNb); 
    tos.print(" "+item.appendedChar); 
    char c = (char) item.appendedChar;
    if (Character.isLetterOrDigit(c))
      tos.print(" \"" + c + "\"");
    else
      tos.print(" " + "-");
    tos.println();
  }

  private static LzvItem readItem(FileInputStream bis, Scanner tis) throws IOException {
    LzvItem res =  readItemFromBinary(bis);    
    LzvItem res1 = readItemFromText  (tis);    
    if (res==null && res1==null) return res;
    if((res==null ^ res1==null) || !res1.equals(res)) {
      System.out.println("Oups... binary and text formats don't agree!");
      System.exit(-1);
    }
    return res;
  }

  /** returns null when there is no item (EOF) */
  private static LzvItem readItemFromBinary(FileInputStream bis) throws IOException {
    return null; // TODO - A COMPLETER    
  }

  private static LzvItem readItemFromText(Scanner tis) throws IOException {
    if (!tis.hasNext()) return null;
    int     code = tis.nextInt();
    byte crtByte = tis.nextByte();
    tis.next();  // dummy character
    LzvItem res = new LzvItem(code, crtByte);
    return res;
  }
  
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String [] args) {
    if (args.length != 3 ) usage();
    String cmd = args[0];
    try {
      if (cmd.equals("code"))
        code(args[1], args[2]);
      else if (cmd.equals("decode"))
        decode(args[1], args[2]);
      else
        usage();
    } catch (IOException e) {
      System.err.println(e);
      e.printStackTrace();
    }
  }
  // ------------------------------------------------------------
  private static void usage() {
    System.out.println("Usage: LempelZiv code   infile outfile");
    System.out.println("   or: LempelZiv decode infile outfile"); 
    System.exit(-1);
  }
}
