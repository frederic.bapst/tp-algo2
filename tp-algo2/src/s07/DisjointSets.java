package s07;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
// ------------------------------------------------------------
public class DisjointSets {
  // TODO - A COMPLETER

  // PRE: nbOfElements >= 0
  public DisjointSets(int nbOfElements) {
    // TODO - A COMPLETER
  }

  /** determines whether i and j are in the same group.
   *  PRE: 0 <= i,j < nbOfElements() */
  public boolean isInSame(int i, int j) {
    return false;     // TODO - A COMPLETER
  }

  /** merges the respective groups containing i and j 
  *   PRE: 0 <= i,j < nbOfElements() */
  public void union(int i, int j) {
    // TODO - A COMPLETER
  }

  public int nbOfElements() {  // as given in the constructor
    return 0; // TODO - A COMPLETER
  }
  
  /** @return the smallest value in the same group as i 
   *  PRE: 0 <= i < nbOfElements() */
  public int minInSame(int i) {
    return 0; // TODO - A COMPLETER
  }

  /** @return true if all elements are in the same group */
  public boolean isUnique() { 
    return false;    // TODO - A COMPLETER
  }

  /** String format like this: "{0}{3,2,4}{1,5}" */ 
  @Override public String toString() {
    // TODO - complete the following code...
    String res = "";
    int n = nbOfElements();
    for(int i=0; i<n; i++) {
      // if(i is not a root) continue;  // TODO
      res += "{" + i;
      Queue<Integer> q = new LinkedList<>();  // for breadth-first traversal
      q.add(i);
      while(!q.isEmpty()) {
        int v = q.remove();
        for(int j=0; j<n; j++) {
          // if(j is a child of v) {    // TODO
          //   add j to res
          //   …
          // }
        }
      }
      res += "}";
    }
    return res;
  }

  /** whether both DisjointSets represent the same logical set of groups */
  @Override public boolean equals(Object otherDisjSets) {
    return false;    // TODO - A COMPLETER
  }
}
