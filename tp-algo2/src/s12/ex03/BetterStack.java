
package s12.ex03;

// Each operation in O(1) CPU worst-case
public class BetterStack {
  // TODO
  public void push(float e) {
    throw new UnsupportedOperationException(); //TODO
  }
  public float pop() {
    throw new UnsupportedOperationException(); //TODO
  }
  public float consultMin() {
    throw new UnsupportedOperationException(); //TODO
  }
  public float consultMax() {
    throw new UnsupportedOperationException(); //TODO
  }
  public boolean isEmpty() {
    throw new UnsupportedOperationException(); //TODO
  }
}
