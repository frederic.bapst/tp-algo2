package s12.ex05;
import java.util.HashSet;
import java.util.Set;

// RAM O(n)
public class AnagramDico {
  // TODO
  
  // worst-case CPU O(n ln n)
  public AnagramDico(String[] words) {
    // TODO
  }
  
  // worst-case CPU O(K + ln n);  K=res.size(), output-sensitive
  public Set<String> anagramsOf(String s) {
    Set<String> res = new HashSet<>();
    // TODO
    return res;
  }

  // ----------------------------------------------------------------------
  static void demo() {
    String[] t = {"spot","zut","pots", "stop", "hello"};
    AnagramDico d = new AnagramDico(t);
    d = new AnagramDico(t);
    var res = d.anagramsOf("tops");
    System.out.println(res); // [stop, spot, pots]
  }
  
  public static void main(String[] args) {
    demo();
  }
}
