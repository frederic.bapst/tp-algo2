
package s12.ex13;

import java.util.Random;

public class CyclicOrNot {
  public static class LNode { 
    LNode next;
    LNode(LNode n) { next=n; }
  }
  // ===============================
  
  // O(n) in CPU, but strict O(1) in RAM
  public static boolean hasLoop(LNode head) {
    return false; // TODO
  }
  
  static LNode makeList(int n, boolean hasLoop, Random rnd) {
    LNode last = new LNode(null);
    LNode first = last;
    for(int i=0; i<n; i++) 
      first = new LNode(first);
    if (hasLoop) {
      int x = rnd.nextInt(n);
      LNode anchor = first;
      for(int i=0; i<x; i++)
        anchor = anchor.next;
      last.next = anchor;
    }
    return first;
  }
  
  public static void demo() {
    Random rnd = new Random();
    LNode a = makeList(20, false, rnd);
    LNode b = makeList(20, true,  rnd);
    System.out.println(hasLoop(a));
    System.out.println(hasLoop(b));
  }

  public static void main(String[] args) {
    demo();
  }
  
}
