
package s12.ex10;

import java.awt.Point;

public class FairPolygonSplitting {
  public record SplitSolution(int from, int to, double cost) {
    // one part is made of corners from, from+1, ..., to
    // cost is abs(perimA - perimB), we want to minimize that
    
    @Override public String toString() {
      return String.format("[%d,%d : %f]", from, to, cost);
    }
  }
  //=============================================
  // Worst-case CPU O(n)
  public static SplitSolution fairSplit(Point[] polygon) {
    return null; // TODO
  }
}
