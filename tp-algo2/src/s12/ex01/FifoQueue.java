
package s12.ex01;

// Implement this class, with amortized O(1) for each operation; 
// The only collection available is Stack (no array, no chaining nodes etc.)
public class FifoQueue<E> {
  public FifoQueue() {
    throw new UnsupportedOperationException(); //TODO
  }
  public void enqueue(E elt) {
    throw new UnsupportedOperationException(); //TODO
  }
  public E dequeue() {
    throw new UnsupportedOperationException(); //TODO
  }
  public E consultOldest() {
    throw new UnsupportedOperationException(); //TODO
  }
  public boolean isEmpty() {
    throw new UnsupportedOperationException(); //TODO
  }
}
