
package s12.ex18;

import java.util.Arrays;
import java.util.Random;

public class FindSuchR {
  
  // Worst-case CPU : far better than O(n)
  public static int findSuchR(int[] t) {
    return 0; // TODO
  }

  static boolean isOk(int[] t, int r) {
    int n=t.length;
    if(n==1)   return r==0;
    if(r==0)   return t[r] <= t[r+1];
    if(r==n-1) return t[r] <= t[r-1];
    return t[r] <= t[r+1] && t[r] <= t[r-1];
  }

  static void testWith(int[] t) {
    int[] u = Arrays.copyOf(t, t.length);
    int r = findSuchR(t);
    if(!Arrays.equals(t, u)) throw new Error("bug - modifies the array!");
    if(!isOk(t, r)) throw new Error("bug - wrong value!");
  }

  static int[] rndArray(int n, Random r) {
    int[] t = new int[n];
    for (int i=0; i<n; i++) 
      t[i] = r.nextInt();
    return t;
  }

  static void smallTest() {
    Random rnd = new Random();
    int n = 7, m = 100;
    for(int i=0; i<m; i++) {
      int[] t = rndArray(n, rnd);
      testWith(t);
    }
    System.out.println("seems Ok...");
  }

  public static void main(String[] args) {
    smallTest();
  }
}
