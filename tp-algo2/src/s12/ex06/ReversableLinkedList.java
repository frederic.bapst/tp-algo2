package s12.ex06;

public final class ReversableLinkedList<E> implements IList<E> {
  // TODO
  
  public ReversableLinkedList() {
    // TODO
  }
  
  @Override
  public int size() {
    return 0;   // TODO
  }
  
  @Override
  public IListItr<E> iterator() {
    return new ReversableLinkedListItr<>(this);
  }
  
  @Override public String toString() {
    String s = "[";
    IListItr<E> li = iterator();
    while(!li.isLast()) {
      s += " " + li.consultAfter();
      li.goToNext();
    }
    return s + " ]";
  }
  
}
