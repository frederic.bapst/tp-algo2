package s12.ex06;

// Similar to the specification we used in Algo1.  
// Every operation in O(1) worst-case CPU.
public final class ReversableLinkedListItr<E> implements IListItr<E> {
  // TODO

  ReversableLinkedListItr(ReversableLinkedList<E> theList) {
    // TODO
  }

  @Override
  public void insertAfter(E e) {
    // TODO
    assert consultAfter() == e;
  }

  @Override
  public void removeAfter() { 
    if(isLast()) throw new IllegalStateException("can't removeAfter when isLast");
    // TODO
  }

  @Override
  public E consultAfter() {
    if(isLast()) throw new IllegalStateException("can't consultAfter when isLast");
    // TODO
    return null;
  }

  @Override
  public void goToNext() { 
    if(isLast()) throw new IllegalStateException("can't goToNext when isLast");
    // TODO
  }

  @Override
  public void goToPrev() {
    if(isFirst()) throw new IllegalStateException("can't goToPrev when isFirst");
    // TODO
  }

  @Override
  public void goToFirst() {
    // TODO
  }
  
  @Override
  public void goToLast() {
    // TODO
  }
  
  @Override
  public boolean isFirst() { 
    return false;    // TODO                
  }
  
  @Override
  public boolean isLast()  {
    return false;    // TODO
  }

  @Override
  public void reverseBefore() {
    // TODO
  }

}
