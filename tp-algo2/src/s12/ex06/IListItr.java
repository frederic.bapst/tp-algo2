package s12.ex06;

// Similar to the specification we used in Algo1.  
// Every operation in O(1) worst-case CPU.
public interface IListItr<E> {
  void insertAfter(E e);
  void removeAfter();
  E   consultAfter();
  void goToNext();
  void goToPrev();
  void goToFirst();
  void goToLast();
  boolean isFirst();
  boolean isLast();
  void reverseBefore();
}