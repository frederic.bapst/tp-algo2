
package s12.ex15;

public class StarsDistribution {
  // Rules:
  // - every cell gets at least one star
  // - when t[i] == t[i+1], both cells get the same number of stars
  // - when t[i] >  t[i+1], cell i gets more  stars than cell i+1
  // - when t[i] <  t[i+1], cell i gets fewer stars than cell i+1
  // Returns the total number of stars needed, according to the given rules. 
  // O(n) in CPU, but strict O(1) in RAM
  public static int nbOfStars(float[] t) {
    return 0; // TODO
  }
  
  static void demo() {
    float[] t = {4,2,2,8,8,3,1,5};
    System.out.println(nbOfStars(t));  // 15
  }
  
  public static void main(String[] args) {
    demo();
  }
}
