
package s12.ex04;

public class FindMindAndMax {
  public record MinAndMax(long min, long max) {}
  //======================================
  // Minimize the number of comparisons between elements (i.e. calls to
  // compareTo() if we adapt for an array of Comparable) :
  // - worst-case: no more than 1.7n comparisons
  // - best-case:  no more than 1.1n comparisons 
  // Note that finding the min needs (n-1) comparisons, thus the naive 
  // algorithm involves 2(n-1) comparisons)
  public static MinAndMax minAndMax(long[] t) {
    throw new UnsupportedOperationException(); // TODO
  }
}
