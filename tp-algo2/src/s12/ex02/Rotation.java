
package s12.ex02;

import java.util.Arrays;

public class Rotation {
  // must be O(n) in CPU, and O(1) in RAM
  public static void rotate(int[] t, int i) {
    throw new UnsupportedOperationException(); //TODO
  }
  
  public static void main(String[] args) {
    int[] t = {0,1,2,3,4};
    rotate(t, 2); 
    System.out.println(Arrays.toString(t)); //  --> [2, 3, 4, 0, 1]
    t = new int[]{0,1,2,3};
    rotate(t, 2); 
    System.out.println(Arrays.toString(t)); //  --> [2, 3, 0, 1]
  }
}
