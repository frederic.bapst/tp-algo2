package s11;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class StackTestJU {
  private IntStack s1, s2;

  @BeforeEach
  public void setUp() {
    s1 = new IntStack(10);
    s2 = new IntStack();
  }

  @Test
  public void testNewIsEmpty() {
    assertTrue(s1.isEmpty() && s2.isEmpty());
  }

  @Test
  public void testPushThenPop() {
    s1.push(4);
    assertEquals(4, s1.pop());
  }
}
