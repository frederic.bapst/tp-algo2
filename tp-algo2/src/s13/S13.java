
package s13;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class S13 {
  
  // ------ Ex. 1 -----------
  public static void printSomePowersOfTwo() {
    // TODO ...
  }

    // ------ Ex. 2 -----------
  public static String macAddress(int[] t) {
    // TODO ...
    return "";
  }
  
  // ------ Ex. 3 -----------

  public static long[] divisors(long n) {
    // TODO ...
    return null;
  }

  // ------ Ex. 4a -----------
  public static long sumOfDivisors(long n) {
    // TODO ...
    return 0L;
  }
  
  
  // ------ Ex. 4b -----------
  public static long[] perfectNumbers(long max) {
    // TODO ...
    return null;
  }
  
  // ------ Ex. 5 -----------
  public static void printMagicWord() throws IOException {
    Path path = FileSystems.getDefault().getPath("wordlist.txt");
    Files.lines(path)
      .filter(x -> x.length() == 11)
      .filter(x -> x.charAt(2) == 't')
      .filter(x -> x.charAt(4) == 'l')
      .filter(x -> x.chars().distinct().count() == 6)
      .forEach(System.out::println);
   }
  
  public static boolean isPalindrome(String str) {
    return str.equals(new StringBuilder(str).reverse().toString());
  }

  public static void printPalindromes() throws IOException {
    // TODO ...
  }

  // ------ Ex. 6 -----------
  public static void averages() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };

    double[] grades = new double[results.length];
    for (int i = 0; i < results.length; i++) {
        double sum = 0;
        for (int j = 0; j < results[i].length; j++) {
            sum += results[i][j];
        }
        grades[i] = 1.0 + sum / 10.0;
    }
    double sum = 0;
    for (int i = 0; i < grades.length; i++) {
        sum += grades[i];
    }
    double average = sum / grades.length;

    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  public static void averagesWithStreams() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };
    
    // TODO ...
    double[] grades = null;
    double average = 0.0;
    
    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  // ------ Ex. 8 -----------

  static DoubleStream sampling(double from, double to, int nSubSamples) {
    return null; // TODO ...
  }
  
  public static void ex8() {    
    // TODO...
    // System.out.println(... product of:  1.2, 3.4, 5.6
    
    // System.out.println(... math series:  i/2^i for i between 4 and 30
    
    // System.out.println(... concatenation of: {"Hello","World","!"}

    // System.out.println(... max of: sin^2*cos in [pi/4..2pi/3], 2002 samples

  }

  // ------ Ex. 9 -----------
  public static void nipas(int n) {
    System.out.println(
      IntStream.range(0, n)
      .mapToObj(
        i -> IntStream.range(i, i+4)
             .mapToObj(j ->
                 " ".repeat(n+2-j) +
                 "*".repeat(1+2*j) )
             .collect(Collectors.joining("\n")))
      .collect(Collectors.joining("\n"))
    );
  }

  //-------------------------------------------------------
  public static void main(String[] args) {
    printSomePowersOfTwo();
    
    System.out.println(macAddress(new int[]{78, 26, 253, 6, 240, 13}));

    System.out.println(Arrays.toString(divisors(496L)));
    
    System.out.println(sumOfDivisors(496L));

    System.out.println(Arrays.toString(perfectNumbers(10_000)));
  
    try {
      printMagicWord();
      printPalindromes();
    } catch(IOException e) {
      System.out.println("!! Problem when reading file... " + e.getMessage());
    }

    averages();
    averagesWithStreams();
    ex8();

    // nipas(4); // read/analyze the code first!...
  }

}
